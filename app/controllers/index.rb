require 'httparty'
require 'json'

get '/' do
  response = HTTParty.get 'http://judging.thehackerolympics.com/leaderboard.json'
  @data = response.parsed_response
  p @data
  erb :index
end

get '/example.json' do
  content_type :json
  { :key1 => 'value1', :key2 => 'value2' }.to_json
end